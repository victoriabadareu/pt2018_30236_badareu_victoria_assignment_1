package View;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import Model.operatii;
import Model.polinom;

@SuppressWarnings("serial")
public class GUI extends JFrame {

	// declararea unui frame
	private JFrame f = new JFrame("Polynomial Calculator");

	/// Declaram panel-urile in care vor fi introduse restul componentelor
	/// (butoane, text field-uri)
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();

	/// Label-uri care vor fi afisate in fereastra
	private JLabel instr1 = new JLabel("Introduceti polinoamele!");

	/// Label-uri pentru datele ce tin de polinomul 1
	private JLabel pol1 = new JLabel("Polinomul 1:");

	/// Label-uri pentru datele ce tin de polinomul 2
	private JLabel pol2 = new JLabel("Polinomul 2:");

	/// Textfield-uri pentru datele care vor fi prelucrate/ afisate
	// Cazul polinomului 1:
	private JTextField polinom1 = new JTextField("");
	/// Cazul polinomului p2:
	private JTextField polinom2 = new JTextField("");
	/// Label-uri pentru datele ce tin de rezultat
	private JLabel pol3 = new JLabel("Rezultat:");

	/// TextArea pentru afisarea rezultatului
	private JTextArea res = new JTextArea();

	/// Butoane pentru efectuarea de operatii
	private JButton add = new JButton("Adunare");
	private JButton sub = new JButton("Scadere");
	private JButton mul = new JButton("Inmultire");
	private JButton div = new JButton("Impartire");
	private JButton der1 = new JButton("Derivare P1");
	private JButton der2 = new JButton("Derivare P2");
	private JButton int1 = new JButton("Integrare P1");
	private JButton int2 = new JButton("Integrare P2");

	public String getPolinom1() {
		return polinom1.getText();
	}

	public String getPolinom2() {
		return polinom2.getText();
	}

	public void setRezultat(String text) {
		res.setText(text);
	}

	public GUI() {
		// setarea butonului X pentru exit
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// initializarea dimensiunilor frame-ului
		f.setSize(560, 640);

		/// Aranjarea componentelor in panel-uri
		p1.add(instr1);
		// p1.add(instr2);
		p1.add(pol1);
		p1.add(polinom1);
		p1.add(pol2);
		p1.add(polinom2);
		p1.add(pol3);

		p1.setLayout(new GridLayout(12, 12));
		p1.setBorder(new EmptyBorder(new Insets(25, 25, 25, 105)));
		f.getContentPane().add(BorderLayout.NORTH, p1);
		f.getContentPane().add(BorderLayout.CENTER, p2);
		p2.setLayout(null);
		add.setBounds(12, 13, 160, 60);
		p2.add(add);
		sub.setBounds(190, 14, 160, 60);
		p2.add(sub);
		mul.setBounds(12, 84, 160, 60);
		p2.add(mul);
		div.setBounds(190, 84, 160, 60);
		p2.add(div);
		der1.setBounds(370, 14, 160, 60);
		p2.add(der1);
		der2.setBounds(370, 84, 160, 60);
		p2.add(der2);
		int1.setBounds(100, 154, 160, 60);
		p2.add(int1);
		int2.setBounds(270, 154, 160, 60);
		p2.add(int2);

		f.getContentPane().add(BorderLayout.CENTER, p2);
		res.setBounds(40, 242, 556, 116);
		p1.add(res);
		res.setLineWrap(true);// in TextArea, rezultatul va aparea pe mai multe
								// linii (in continuare)
		f.getContentPane().add(BorderLayout.SOUTH, p3);

		// face interfata vizibila
		f.setVisible(true);

		add.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String a = getPolinom1();
				String b = getPolinom2();
				polinom p1 = new polinom();
				polinom p2 = new polinom();
				p1 = p1.nou(a);
				p2 = p2.nou(b);
				operatii o = new operatii();
				p1 = o.adunare_polinoame(p1, p2);
				String c = p1.toString();
				setRezultat(c);
			}
		});

		sub.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String a = getPolinom1();
				System.out.println(a);
				String b = getPolinom2();
				polinom p1 = new polinom();
				polinom p2 = new polinom();
				p1 = p1.nou(a);
				p2 = p2.nou(b);
				operatii o = new operatii();
				p1 = o.scadere_polinoame(p1, p2);
				String c = p1.toString();
				setRezultat(c);
			}
		});

		mul.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String a = getPolinom1();
				String b = getPolinom2();
				polinom p1 = new polinom();
				polinom p2 = new polinom();
				p1 = p1.nou(a);
				p2 = p2.nou(b);
				operatii o = new operatii();
				p1 = o.inmultire_polinoame(p1, p2);
				String c = p1.toString();
				setRezultat(c);
			}
		});

		der1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String a = getPolinom1();
				// String b = getPolinom2 ();
				polinom p1 = new polinom();
				// polinom p2=new polinom ();
				p1 = p1.nou(a);
				// p2=p2.nou (b);
				operatii o = new operatii();
				p1 = o.derivare(p1);
				String c = p1.toString();
				setRezultat(c);
			}
		});

		der2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				String b = getPolinom2();
				polinom p2 = new polinom();
				p2 = p2.nou(b);
				operatii o = new operatii();
				p2 = o.derivare(p2);
				String c = p2.toString();
				setRezultat(c);
			}
		});

		int1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				String a = polinom1.getText();
				System.out.println(a);
				polinom p1 = new polinom();
				 p1=p1.nou(a);
				System.out.println("Hei" +p1.toString());
				operatii o = new operatii();
				p1 = o.integrare(p1);
				
				String c = p1.toString();
				System.out.println(c);
				setRezultat(c);
			}
		});

		int2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String b = getPolinom2();
				polinom p2 = new polinom();
				p2 = p2.nou(b);
				operatii o = new operatii();
				p2 = o.integrare(p2);
				String c = p2.toString();
				setRezultat(c);
			}
		});

	}
}
