package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import Model.monom;
import Model.operatii;
import Model.polinom;

public class testcase {
	
	

	
	@Test
	public void TestAdunare() {
		
		polinom p1 = new polinom();
		polinom p2 = new polinom();
		polinom p3 = new polinom();
		operatii o = new operatii();
		
		p1.add(new monom(1, 2));
		p1.add(new monom(2, 3));
		p1.add(new monom(0, 3));
		p2.add(new monom(1, 3));
		p2.add(new monom(2, 1));
		p2.add(new monom(0, 4));
		
		
		polinom p4 = new polinom();
		p4.add(new monom(1, 5));
		p4.add(new monom(2, 4));
		p4.add(new monom(0, 7));
		System.out.println(p4.toString());

		p3 = o.adunare_polinoame(p1, p2);
		System.out.println(p3.toString());
		assertEquals(p3.toString(), p4.toString());
	}
}
