package Model;

import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class polinom {

	private  ArrayList<monom> p = new ArrayList<monom>();

	public polinom(ArrayList<monom> p) {
		this.p = p;
	}

	public polinom() {
		// TODO Auto-generated constructor stub
	}

	public void setPolinom(ArrayList<monom> polinom) {

		this.p = polinom;
	}

	public void add(monom m) {
		p.add(m);
	}

	public ArrayList<monom> getPolinom() {
		return p;
	}

	public String toString() {
		String rezultat = "";

		for (monom i : p) {
			int grad = i.getGrad();
			double coeficient = i.getCoeficient();

			// cazul 1: grad=0
			// a) coeficient<0
			if (grad == 0) {

				if (coeficient < 0) {
					rezultat = rezultat + "+" + coeficient;
				} else {
					// b) coeficient>0
					if (coeficient == 0)

					{
						rezultat = rezultat + "0";
					}

					else // c) coeficient=0
					{
						rezultat = rezultat + "+" + coeficient;
					}
				}
			}

			// cazul 2: grad>0
			if (grad > 0) {
				// a) coeficient<0
				if (coeficient < 0) {
					rezultat = rezultat + "+" + i.toString();
				} else {
					if (coeficient == 0) // b) coeficient=0
					{
						rezultat = rezultat + "0";
					} else {
						// c) coeficient>0

						rezultat = rezultat + "+" + i.toString();
					}
				}

			}

		}

		return rezultat;
	}

	// am creat un pattern pentru a identifica monoame in Stringul introdus
	// ca parametru
	// (-?\\b\\d+) identifica coeficientu si semnul acestuia = group(1)
	// [xX]\\^ identifica daca intre coeficient si grad este x^ sau X^
	// (-?\\d+\\b) identifica gradu = group(2)
	// daca am gasit un monom sub forma de strig construim un nou monom
	// convertim gradul respectiv coeficientul din string in int respectiv
	// double
	// si is adaugam intr-un Polinom
	public polinom nou(String string) {
		polinom polinom = new polinom();

		Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");

		Matcher m = p.matcher(string);

		while (m.find()) {
			int grad;
			double coef;

			grad = Integer.parseInt(m.group(2));
			coef = Double.parseDouble(m.group(1));

			monom monom = new monom(grad, coef);

			polinom.getPolinom().add(monom);
		}

		return polinom;
	}

}
