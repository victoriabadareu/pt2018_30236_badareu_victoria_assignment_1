package Model;

import java.util.ArrayList;

public class operatii {

	
	public polinom  aranjare(polinom p )
	{
		for (int i = 0; i < p.getPolinom().size(); i++) {
			for (int j = i+1; j < p.getPolinom().size(); j++) {
				int grad1 = p.getPolinom().get(i).getGrad();
				int grad2 = p.getPolinom().get(j).getGrad();
				if(grad1==grad2)
				{	double coeficient1 = p.getPolinom().get(i).getCoeficient();
					double coeficient2 = p.getPolinom().get(j).getCoeficient();
					p.getPolinom().get(i).setCoeficient(coeficient1 + coeficient2);
					p.getPolinom().remove(j);
				}
		
			}
		}
		
		
		
		
		return p;
	}
	
	// adunarea polinoamelor

	public polinom adunare_polinoame(polinom p1, polinom p2) {
		// int n=p1.getPolinom().size();
		polinom p3 = new polinom();
		for (int i = 0; i < p1.getPolinom().size(); i++) {
			for (int j = 0; j < p2.getPolinom().size(); j++) {
				int grad1 = p1.getPolinom().get(i).getGrad();
				int grad2 = p2.getPolinom().get(j).getGrad();
				
				if (grad1 == grad2) {
					
					double coeficient1 = p1.getPolinom().get(i).getCoeficient();
					double coeficient2 = p2.getPolinom().get(j).getCoeficient();
					
					monom aux = new monom(grad1, coeficient1 + coeficient2);
					p3.add(aux);
					
					p2.getPolinom().remove(j);
					p1.getPolinom().remove(i);
					j--;
					i--;
				}
			}
		}

		for (int i = 0; i < p1.getPolinom().size(); i++) {
			monom aux = new monom(p1.getPolinom().get(i).getGrad(), p1.getPolinom().get(i).getCoeficient());
			p3.add(aux);
			p1.getPolinom().remove(i);
			i--;
		}
		for (int j = 0; j < p2.getPolinom().size(); j++) {
			monom aux1 = new monom(p2.getPolinom().get(j).getGrad(), p2.getPolinom().get(j).getCoeficient());
			p3.add(aux1);
			p2.getPolinom().remove(j);

			j--;

		}

		return p3;
	}

	// scaderea polinoamelor
	public polinom scadere_polinoame(polinom p1, polinom p2) {// negam toti
																// coeficientii
																// polinomului
																// doi si facem
																// adunare
		for (int k = 0; k < p2.getPolinom().size(); k++) {
			p2.getPolinom().get(k).setCoeficient(0 - p2.getPolinom().get(k).getCoeficient());
		}
		polinom p3 = new polinom();
		operatii o = new operatii();
		p3 = o.adunare_polinoame(p1, p2);
		return p3;
	}

	// inmultirea polinoamelor
	public polinom inmultire_polinoame(polinom p1, polinom p2) {
		polinom p3 = new polinom();
		for (int i = 0; i < p1.getPolinom().size(); i++) {
			for (int j = 0; j < p2.getPolinom().size(); j++) {
				monom aux = new monom(p1.getPolinom().get(i).getGrad() + p2.getPolinom().get(j).getGrad(),
						p1.getPolinom().get(i).getCoeficient() * p2.getPolinom().get(j).getCoeficient());

				p3.add(aux);
				// p2.getPolinom().remove(j);

			}

		}

		for (int i = 0; i < p3.getPolinom().size(); i++) {
			for (int j = i + 1; j < p3.getPolinom().size(); j++) {
				if (p3.getPolinom().get(i).getGrad() == p3.getPolinom().get(j).getGrad())

				{
					p3.getPolinom().get(i).setCoeficient(
							p3.getPolinom().get(i).getCoeficient() + p3.getPolinom().get(j).getCoeficient());
					p3.getPolinom().remove(j);
				}

			}
		}
		return p3;
	}

	// impartirea polinoamelor
	/*
	 * public polinom impartire_polinoame(polinom p1, polinom p2) { polinom
	 * cat=new polinom(); polinom rest=new polinom(); for (int i=0;
	 * i<p1.getPolinom().size(); i++) { for(int j=0; j<p2.getPolinom().size();
	 * j++) { monom aux = new
	 * monom(p1.getPolinom().get(i).getGrad()-p2.getPolinom().get(j).getGrad(),
	 * p1.getPolinom().get(i).getCoeficient()/p2.getPolinom().get(j).
	 * getCoeficient());
	 * 
	 * p3.add(aux); } }
	 * 
	 * return cat; }
	 */
	// derivarea polinomului
	public polinom derivare(polinom p1) {
		polinom p3 = new polinom();
		for (int i = 0; i < p1.getPolinom().size(); i++) {
			if (p1.getPolinom().get(i).getGrad() != 0) {

				monom aux = new monom(p1.getPolinom().get(i).getGrad() - 1,
						p1.getPolinom().get(i).getCoeficient() * p1.getPolinom().get(i).getGrad());
				p3.add(aux);

			}

			/*
			 * else { //daca gradul este 0, atunci polinomul este o constanta,
			 * iar derivata acestuia este 0 monom aux=new monom (0, 0);
			 * p3.add(aux);
			 * 
			 * }
			 */
		}
		return p3;
	}

	// integrarea polinomului
	public polinom integrare(polinom p1) {
		polinom p3 = new polinom();
		for (int i = 0; i < p1.getPolinom().size(); i++) {
			// if (p1.getPolinom().get(i).getGrad()>0)

			// {
			monom aux = new monom(p1.getPolinom().get(i).getGrad() + 1,
					p1.getPolinom().get(i).getCoeficient() * (1.00 / (p1.getPolinom().get(i).getGrad() + 1)));
			p3.add(aux);

			// }
			/*
			 * else if (p1.getPolinom().get(i).getGrad()==0) { monom aux=new
			 * monom(0,0); p3.add(aux); }
			 */
		}
		return p3;

	}
}
