package Model;

public class monom {

	private int grad;
	private double coeficient;

	public monom(int grad, double d) {
		this.grad = grad;
		this.coeficient = d;

	}

	public monom() {
		// TODO Auto-generated constructor stub
	}

	public void setGrad(int grad) {
		this.grad = grad;

	}

	public void setCoeficient(double coeficient) {
		this.coeficient = coeficient;
	}

	public int getGrad() {
		return grad;

	}

	public double getCoeficient() {
		return coeficient;
	}

	public String toString() {
		String rezultat = "";

		if (getCoeficient() > 0) {
			rezultat = getCoeficient() + "x^" + getGrad();
		} else if (getCoeficient() < 0) {
			rezultat = "(" + getCoeficient() + ")" + "x^" + getGrad();

		}
		return rezultat;
	}

}
